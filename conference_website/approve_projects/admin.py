from django.contrib import admin

from .models import Allowance, Project, Verdict, User


@admin.register(Allowance)
class AllowanceAdmin(admin.ModelAdmin):
    list_display = (
        "pk",
        "project",
        "allowance",
    )


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = (
        "pk",
        "title",
        "last_name",
        "first_name",
        "patronymic",
        "description",
    )


@admin.register(Verdict)
class VerdictAdmin(admin.ModelAdmin):
    list_display = (
        "pk",
        "project",
        "checker",
        "verdict",
    )


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = (
        "pk",
        "username",
        "last_name",
        "first_name",
        "patronymic",
        "email",
        "telegram",
        "uid",
    )
