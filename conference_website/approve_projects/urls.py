from django.urls import path

from . import views
from .views import ProjectDetailView, ProjectListView

app_name = "approve_projects"

urlpatterns = [
    path("", views.index, name="index"),
    path("project_list/", ProjectListView.as_view(), name="project_list"),
    path("projects/<int:pk>/", ProjectDetailView.as_view(), name="project_detail"),
    path("check_projects/", views.check_projects, name="check_projects"),
    path("<str:user_uid>/", views.checker_login, name="checker_login"),
]
