from django.apps import AppConfig


class ApproveProjectsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "approve_projects"
