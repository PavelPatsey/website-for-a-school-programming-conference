from django.db import models
from django.contrib.auth.models import AbstractUser

VERDICT_CHOICES = (
    ("допустить", "допустить"),
    ("отказать", "отказать"),
)

ALLOWANCE_CHOICES = (
    ("ждет проверки", "ждет проверки"),
    ("допущен", "допущен"),
    ("отказано", "отказано"),
)


class Project(models.Model):
    last_name = models.CharField(max_length=100)
    first_name = models.CharField(max_length=100)
    patronymic = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    description = models.TextField()

    def __str__(self):
        return self.title

    def is_allowed(self):
        return sum(1 for x in self.verdicts if x.verdict == VERDICT_CHOICES[0][0]) == 3


class Allowance(models.Model):
    project = models.ForeignKey(
        Project,
        on_delete=models.CASCADE,
        related_name="allowances",
    )
    allowance = models.CharField(
        max_length=100,
        choices=ALLOWANCE_CHOICES,
        default="ждет проверки",
        verbose_name="Допуск",
    )

    def __str__(self):
        return f"{self.project} {self.allowance}"


class User(AbstractUser):
    patronymic = models.CharField("patronymic", max_length=100, blank=True)
    telegram = models.CharField("telegram", max_length=100, unique=True, blank=True)
    uid = models.TextField(max_length=500, unique=True)


class Verdict(models.Model):
    project = models.ForeignKey(
        Project,
        on_delete=models.CASCADE,
        related_name="verdicts",
    )
    checker = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="verdicts",
    )
    verdict = models.CharField(
        max_length=100,
        choices=VERDICT_CHOICES,
        verbose_name="Вердикт",
    )

    def __str__(self):
        return f"{self.project} {self.checker} {self.verdict}"
