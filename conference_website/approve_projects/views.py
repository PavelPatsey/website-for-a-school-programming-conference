from collections import Counter

from django.db.models import Count
from django.forms import formset_factory
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.views.generic import DetailView, ListView

from .forms import VerdictForm
from .models import Allowance, Project, Verdict, User
from django.contrib.auth import login

from django.contrib.auth.decorators import login_required


VERDICT_TO_ALLOWANCE = {
    "допустить": "допущен",
    "отказать": "отказано",
}


def index(request):
    return HttpResponse("Стартовая страница приложения Утвердить проекты")


class ProjectListView(ListView):
    queryset = Project.objects.order_by("title")
    context_object_name = "project_list"
    template_name = "projects/project_list.html"


class ProjectDetailView(DetailView):
    model = Project
    template_name = "projects/project_detail.html"
    context_object_name = "project"


def calculate_allowances():
    """Создает Allowance для проектов у которых нет Allowance.
    Для проектов у которых есть хотя бы три вердикта - считаем допуск.
    """
    projects = Project.objects.all()
    for project in projects:
        if not project.allowances.exists():
            Allowance.objects.create(
                project=project,
            )
    projects = (
        Project.objects.annotate(num_verdicts=Count("verdicts"))
        .filter(num_verdicts__gte=3)
        .filter(allowances__allowance="ждет проверки")
    )
    for project in projects:
        verdicts = project.verdicts.order_by("id")
        verdicts_values = []
        for verdict in verdicts:
            verdicts_values.append(verdict.verdict)
        counter = Counter(verdicts_values)
        if max(counter.values()) >= 2:
            result_verdict = max(counter, key=counter.get)
            allowance = Allowance.objects.get(project=project)
            allowance.allowance = VERDICT_TO_ALLOWANCE[result_verdict]
            allowance.save()


def get_unapproved_project(request):
    calculate_allowances()
    try:
        return (
            Project.objects.filter(allowances__allowance="ждет проверки")
            .exclude(verdicts__checker=request.user)
            .annotate(num_verdicts=Count("verdicts"))
            .order_by("num_verdicts")[0]
        )
    except IndexError:
        return


@login_required
def check_projects(request):
    user = get_object_or_404(User, id=request.user.id)
    project = get_unapproved_project(request)

    if request.method == "POST":
        form = VerdictForm(request.POST)
        if form.is_valid():
            Verdict.objects.create(
                project=project,
                checker=user,
                verdict=form.cleaned_data["verdict"],
            )
            return redirect("/approve_projects/check_projects/")
    else:
        form = VerdictForm()
    return render(
        request,
        "approve_projects/check_projects.html",
        {"form": form, "project": project},
    )


def checker_login(request, user_uid):
    user = get_object_or_404(User, uid=user_uid)
    login(request, user)
    return HttpResponse(f"Вы залогинились. Имя пользователя {user.username}")
