# Generated by Django 4.0.4 on 2022-06-12 18:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("approve_projects", "0003_alter_verdict_checker_delete_checker"),
    ]

    operations = [
        migrations.AlterField(
            model_name="allowance",
            name="allowance",
            field=models.CharField(
                choices=[
                    ("ждет проверки", "ждет проверки"),
                    ("допущен", "допущен"),
                    ("отказано", "отказано"),
                ],
                default="ждет проверки",
                max_length=100,
                verbose_name="Допуск",
            ),
        ),
        migrations.AlterField(
            model_name="verdict",
            name="verdict",
            field=models.CharField(
                choices=[("допустить", "допустить"), ("отказать", "отказать")],
                max_length=100,
                verbose_name="Вердикт",
            ),
        ),
    ]
