from django import forms

from .models import Verdict


class VerdictForm(forms.ModelForm):
    class Meta:
        model = Verdict
        fields = ("verdict",)
